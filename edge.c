#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>

#define MAX_DATA 4096
#define MAX_CLIENTS 4
#define UDP_PORT 24084
#define UDP_PORT_AND 22084
#define UDP_PORT_OR 21084
#define TCP_PORT 23084

int main(int argc, char **argv)
{
	int sockfd,clientfd,udpfd;//socket discriptor 
	struct sockaddr_in server,client,edge,backend_and,backend_or;//socket address structure 
	int n;// variable for storing number of Bytes received/sent
	socklen_t addr_size, sockaddr_len = sizeof(struct sockaddr_in);
	char cli_receive_buffer[MAX_DATA]={'\0'},and_recv_buffer[1024] = {'\0'},or_recv_buffer[1024] = {'\0'},send_buffer[512]={'\0'},result_buffer[1024]={'\0'};
	struct sockaddr_storage server_and_Storage,server_or_Storage;// connector's address information
	int i,k;//index variables 
	int line_count_client=0,line_count_and=0,line_count_or=0,flag=0,flag1=0,ssopt,reuse=1;
	FILE *fp='\0',*fp1='\0';//File pointers
	char bootup_message[]={"\nThe Edge Server is up and running."};
	char c = '\0', d = '\0' ;
	
	
	sockfd = socket(AF_INET,SOCK_STREAM,0); // Create a TCP socket 
	
	//Error handle
	if(sockfd < 0)
	{
		perror("socket:");
		exit(-1);
	}
	
	//TCP socket config settings
	server.sin_family = AF_INET;
	server.sin_port = htons(TCP_PORT);
    inet_pton(AF_INET, "127.0.0.1", &server.sin_addr.s_addr);// localhost i.e 127.0.0.1 is hardcoded 
	bzero(&server.sin_zero, 8);

	//Server-And Address
	backend_and.sin_family = AF_INET;
	backend_and.sin_port = htons(UDP_PORT_AND);
	inet_pton(AF_INET, "127.0.0.1", &backend_and.sin_addr.s_addr);
	bzero(&backend_and.sin_zero, 8);

	//Server-Or Address
	backend_or.sin_family = AF_INET;
	backend_or.sin_port = htons(UDP_PORT_OR);
	inet_pton(AF_INET, "127.0.0.1", &backend_or.sin_addr.s_addr);
	bzero(&backend_or.sin_zero, 8);

	edge.sin_family = AF_INET;
	edge.sin_port = htons(UDP_PORT);
	inet_pton(AF_INET, "127.0.0.1", &edge.sin_addr.s_addr);
	bzero(&edge.sin_zero, 8);

	/*Initialize size variable to be used later on*/
	addr_size = sizeof server_and_Storage;


	//port already used error handle
	ssopt = setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR,&reuse, sizeof (reuse));
		if(ssopt)
		{
			perror("ssopt:");
			exit(-1);
		}
	
	
	//Bind the TCP socket to the  address
	if(bind(sockfd,(struct sockaddr *)&server, sockaddr_len) < 0)
	{
		perror("bind");
		exit(-1);
	}
	
	//keeping listening for incoming TCP connection
	while(1)
	{
	
		//listen on TCP PORT 23084
		if(listen(sockfd, MAX_CLIENTS) < 0)
		{
			perror("listen");
			exit(-1);
		}
		//Display the bootup message
		write(1,bootup_message,strlen(bootup_message));

		//Accpet the incoming the connection		
		clientfd = accept(sockfd, (struct sockaddr *)&client, &sockaddr_len);
		if(clientfd < 0)
		{
			perror("accept");
			exit(-1);
		}
		

		//Open a new file to store the data received from the client
		fp = fopen("Client_Data.txt", "w+");
		
    	// Check if file exists
    	if (fp == NULL)
    	{
        	printf("Could not open file ");
        	return 0;
    	}

    	//clear the receive buffer
		memset(cli_receive_buffer,'\0',MAX_DATA-1);

		//Receive data from the client
		n = read(clientfd, cli_receive_buffer, MAX_DATA-1);	
			
			if(n < 0)
			{
				perror("receive");
				exit(-1);
			}

			fprintf(fp, "%s",cli_receive_buffer);
			fclose(fp);

			//Open the received data file to count the number of lines received 
			fp = fopen("Client_Data.txt", "r");

		    if (fp == NULL)
    		{
        		printf("Could not open file ");
        		return 0;
    		}

			line_count_client=0;
			
			do
		   	{
		   		c = fgetc(fp);
		   		if(c == '\n')
		   		line_count_client++;
		   		if(c=='\n' && d=='\n')
		   			line_count_client--;
		   		d=c;
		   	}while(c != EOF);
		   	fclose(fp);
	
			printf("\nThe edge Server has received the %d lines from the client using TCP over port %d", line_count_client, ntohs(client.sin_port));
				

			//Create a UDP socket
			udpfd = socket(AF_INET,SOCK_DGRAM,0);
	
			//Bind the UDP socket to the edge server address
			if(bind(udpfd,(struct sockaddr *)&edge, sockaddr_len) < 0)
			{
				perror("bind");
				exit(-1);
			}
			
		
			fp = fopen("Client_Data.txt", "r");
			
			if (fp == NULL)
    		{
        		printf("Could not open file ");
        		return 0;
    		}

    		// Create a new file to store the computatioin results
			fp1 = fopen("Results.txt", "w");
			
			if (fp1 == NULL)
    		{
        		printf("Could not open file ");
        		return 0;
    		}
    		
    		line_count_and=0;
    		line_count_or=0;
    		flag1=0;
			
			while(1)
			{	
				memset(send_buffer,'\0',511);
				
				if(fgets(send_buffer,512,fp) == NULL) 
					flag1=1;
				//at the end of the file send a dummy charater indicationg end of transmission
				if(flag1==1)
				{
					sendto(udpfd,"b" ,1 , 0,(struct sockaddr *)&backend_and, sockaddr_len);
					sendto(udpfd,"b",1, 0,(struct sockaddr *)&backend_or, sockaddr_len);
					break;
				}
				//If it's AND operation send it to the Server-AND and receive the result and write it to the results file
				if(send_buffer[0] == 'a')
				{
					

					n = sendto(udpfd, send_buffer, strlen(send_buffer), 0,(struct sockaddr *)&backend_and, sockaddr_len);	
		
					if(n < 0)
					{
						perror("Send:");
						exit(-1);	
					}
					line_count_and++;

					memset(and_recv_buffer,'\0',1023);
					
					n = recvfrom(udpfd, and_recv_buffer, 1023, 0,(struct sockaddr *)&server_and_Storage, &addr_size);

					if(n < 0)
					{
						perror("Receive:");
						exit(-1);	
					}
					fwrite(and_recv_buffer, 1, n, fp1);
					fprintf(fp1,"\n");
					
				}
				//If it's OR operation send it to the Server-OR and receive the result and write it to the results file
				if(send_buffer[0] == 'o')
				{
					

					n = sendto(udpfd, send_buffer, strlen(send_buffer), 0,(struct sockaddr *)&backend_or, sockaddr_len);	
					
					if(n < 0)
					{
						perror("Send error");
						exit(-1);	
					}
					line_count_or++;
					memset(or_recv_buffer,'\0',1023);

					n = recvfrom(udpfd, or_recv_buffer, 1023, 0,(struct sockaddr *)&server_or_Storage, &addr_size);

					if(n < 0)
					{
						perror("Receive error");
						exit(-1);	
					}
					fwrite(or_recv_buffer, 1, n, fp1);
					fprintf(fp1,"\n");
				}
				
			}
			fclose(fp);
			fclose(fp1);
			printf("\nThe edge server has successfully sent %d lines to the Backend-Server OR",line_count_or);
			printf("\nThe edge server has successfully sent %d lines to the Backend-Server AND",line_count_and);
			printf("\nThe edge server start receiving the computation results from the Backend-Server OR and Backend-Server AND using UDP port no: %d\nThe computation results are:\n",UDP_PORT);
			
			
			//print the received computation results
			fp1 = fopen("Results.txt","r");
			
			while(1)
			{
				if(fgets(send_buffer,512,fp1)==NULL)
					break;
				printf("%s", send_buffer);
				memset(send_buffer,'\0',511);
			}

			fclose(fp1);
			close(udpfd);
			printf("The edge Server has successfully finished receiving all the computation results from the Backend-Server OR and Backend-Server AND.");

			//separate the result from the operands and operator, i.e. 110 and 11 = 10, separate 10 from the rest of the string

			fp1 = fopen("Results.txt","r");
			
			k=0;
			memset(result_buffer,'\0',1023);
			while(1)
			{
				memset(send_buffer,511,'\0');
				
				flag=0;
				if(fgets(send_buffer,512,fp1)==NULL)
					break;
				for(i=0;send_buffer[i]!='\0';i++)
				{
					if(send_buffer[i] == '=')
					{
						flag=1;
						continue;						
					}
					if(flag==1)
					{
						result_buffer[k]=send_buffer[i];
						k++;
					}				
				}
				result_buffer[k] =='\n';

				
			}
		
			//send the result back to the client 
			n = send(clientfd, result_buffer, strlen(result_buffer), 0);
				if(n<0)
				{
					perror("send:");
					exit(-1);
				}

			fclose(fp1);

			printf("\nThe edge Server has successfully finished sending all the computation results to the client.\n");
			close(clientfd);
						
	}

}

