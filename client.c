#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h> 
#include <errno.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/wait.h>


#define MAX_DATA 4096
#define PORT_NO 23084

int main(int argc, char **argv)
{
	int client_ds;//socket discriptor
	struct sockaddr_in edge_server;//socket address structure
	socklen_t addr_size, sockaddr_len = sizeof(struct sockaddr_in);
	int n;// variable for storing number of Bytes received/sent
	char input_buffer[MAX_DATA] = {'\0'};
	char output_buffer[MAX_DATA]= {'\0'};
	FILE *fp;//file pointer
	char c='\0',d='\0';
	int line_count=0,flag=0,i=0;

	//Create a TCP socket
	client_ds = socket(AF_INET,SOCK_STREAM, 0);

	if(client_ds < 0)
	{
		perror("socket:");//error handle
		exit(-1);
	}
	
	/*Configure settings in remote_server struct*/
	edge_server.sin_family = AF_INET;
	edge_server.sin_port = htons(PORT_NO);
	inet_pton(AF_INET, "127.0.0.1", &edge_server.sin_addr.s_addr);
	bzero(&edge_server.sin_zero, 8);

	printf("\nThe client is up and running.");

	
	//Connect to the Edge server
	if(connect(client_ds, (struct sockaddr *)&edge_server, sockaddr_len) < 0)
	{
		perror("connect");
		exit(-1);
	}
	

	// opent the input data file and send data to the Edge server
	fp = fopen(argv[1], "r");
 
    // Check if file exists
    if (fp == NULL)
    {
        printf("Could not open file ");
        return 0;
    }
 

	 n = fread(input_buffer, MAX_DATA-1, 1, fp);
  	if(n < 0)
   	{
   		perror("File Read:");
   		exit(-1);
   	}
  
    
    n =send(client_ds, input_buffer, strlen(input_buffer), 0);
   	if(n < 0)
   	{
   		perror("Send:");
   		exit(-1);
   	}
  
    fclose(fp);

    //count the number of lines sent to the edge server
	fp = fopen(argv[1], "r");
	line_count=0;
	d = fgetc(fp);

	do
   	{
   		c = fgetc(fp);
   		if(c == '\n')
   		line_count++;
   		if(c=='\n' && d=='\n')
   			line_count--;
   		d=c;
   	}while(c != EOF);
   	fclose(fp);

    printf("\nThe client has successfully finished sending %d lines to the edge server. ",line_count);

    //Receive the computation results back from the edge server
   	n = read(client_ds, output_buffer, MAX_DATA-1-1);

   	if(n < 0)
   	{
   		perror("Receive:");//error handle
   		exit(-1);
   	}
  	
  	printf("\nThe client has successfully finished receiving all computation results from the edge server.\nThe final computation results are:");
  	printf("\n");
  	write(1,output_buffer,strlen(output_buffer));

	close(client_ds);
	return(0);

}

	

