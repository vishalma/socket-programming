#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h> 
#include <errno.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/wait.h>

#define PORT_NO 21084

int main()
{
  int udpSock;//socket discriptor
  int n=0;// variable for storing number of Bytes received/sent
  char buffer[1024];// buffer for receiving incoming data
  struct sockaddr_in serverAddr;//socket address structure
  struct sockaddr_storage serverStorage;//connector's address information
  socklen_t addr_size;
  int i,j,k,l;//index variables
  int flag,flag1,flag2,line_count=0;
  char op1[50] = {'\0'}, op2[50] = {'\0'},operand1[50]={'\0'},operand2[50]={'\0'},result[50]={'\0'},result1[50]={'\0'};
  char bootup_message1[]={"The Server OR is up and running using UDP on port 21084."};
  char bootup_message2[]={"\nThe Server OR start receiving lines from the edge server for OR computation.\nThe computation results are:"};
 
  
  /*Create UDP socket*/
  udpSock = socket(PF_INET, SOCK_DGRAM, 0);

  if(udpSock < 0)
  {//error handle 
    perror("socket:");
    exit(-1);
  }

  /*Configure settings in address struct*/
  serverAddr.sin_family = AF_INET;
  serverAddr.sin_port = htons(PORT_NO);
  serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
  memset(serverAddr.sin_zero, '\0', sizeof serverAddr.sin_zero);  


  /*Bind socket with address struct*/
  if(bind(udpSock, (struct sockaddr *) &serverAddr, sizeof(serverAddr)))
  {
  	perror("Bind:");
  	exit(-1);
  }

  /*Initialize size variable to be used later on*/
  addr_size = sizeof serverStorage;

  //Keep looking for any in connection
  while(1)
  {
  
  //Display the bootup message
  write(1,bootup_message1,strlen(bootup_message1));

  	line_count=0;
  	flag1=0;
	while(1)
  	{
	   //clear all the buffers
	 	memset(buffer,'\0',1023);
	    memset(op1,'\0',49);
	    memset(op2,'\0',49);
	    memset(operand1,'\0',49);
	    memset(operand2,'\0',49);
	    memset(result,'\0',49);
	    memset(result,'\0',49);
	    
  //Receive data from the Edge Server
	    n = recvfrom(udpSock,buffer,1023,0,(struct sockaddr *)&serverStorage, &addr_size);

		if(n<0)
	    {
	    	perror("receive:");
	    	exit(-1);
	    }

	    if (flag1==0)
	    {
	    	write(1,bootup_message2,strlen(bootup_message2));/*Display the second bootup message once the server start's receiving the data from the edge server*/
	    	flag1=1;
	    }
		
		//Stop receiving the data from the edge server, once it has sent all the data.
		if(buffer[0]=='b')
	    	break;	

	    line_count++; // line counter to keep track of number of lines received from the edge server  
		
	      //Extract the two operands from the received line of string from the edge server into op1 and op2
	    flag=0;k=0;l=0;
	    for (j=0;j<n;j++)
	    {
	    	if(buffer[j] ==',')
	    	{
	    		flag++;
	    		continue;
	    	}
	    	if(flag==1)
	    	{
	    		op1[k]=buffer[j];
	    		k++;
	    	}
	    	if(flag==2)
	    	{
	    		op2[l]=buffer[j];
	    		l++;
	    	}

	    }
	    
	    op2[strlen(op2)-1]='\0';
	    k = strlen(op1);
	    l = strlen(op2);
	      //zero pad the smaller operand
	    if(k>l)
	    {
	    	for(j=0;j<k-l;j++)
	    		{operand2[j]='0';}
	    	strcat(operand2,op2);
	    	strcpy(operand1,op1);
	    }
	    if(l>k)
	    {
	    	for(j=0;j<l-k;j++)
	    		{operand1[j]='0';}
	    	strcat(operand1,op1);
	    	strcpy(operand2,op2);
	    }
	    else if(k==l)//else copy the same into operand1 and operand2
	    {
	 		strcpy(operand1,op1);
	 		strcpy(operand2,op2);  	
	    }
	    //Perform AND operation and remove the padded zeros
	    /*for(i=0;i<strlen(operand1);i++)
	    {
	    	result[i] = operand2[i] | operand1[i];
	    }*/

	   flag2=0;
      k=0;
      for(i=0;i<strlen(operand1);i++)
      {
        
        if((operand2[i] | operand1[i])=='1')
        {
          flag2=1;
        }
        if(flag2==1)
        {
          result[k]=operand2[i] | operand1[i];
          k++;
        }
        if(flag2==0)
        {
          result[0]='0';
        }

      }
	    
 		//Display the results
         printf("\n%s or %s = %s", op1,op2,result);
	    strcpy(result1,op1);
	    strcat(result1," or ");
	    strcat(result1,op2);
	    strcat(result1," = ");
	    strcat(result1,result);

	    n = sendto(udpSock,result1,strlen(result1),0,(struct sockaddr *)&serverStorage,addr_size); //Send the result back to the edge server
     

	    if(n<0)
	    {
	    	perror("send:");//error handle
	    	exit(-1);
	    }

    }

  printf("\nThe Server OR has successfully received %d lines from the edge server and finished all OR computations.",line_count);
  printf("\nThe Server OR has successfully finished sending all computation results to the edge server.\n");
}
  close(udpSock);
  return 0;
}