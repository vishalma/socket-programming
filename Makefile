all: 
	gcc -o edgeoutput edge.c
	gcc -o server_oroutput  server_or.c
	gcc -o server_andoutput  server_and.c
	gcc -o clientoutput  client.c
	

.PHONY: edge server_or server_and

server_or: 
	./server_or
server_and: 
	./server_and
edge:   
	./edge 
